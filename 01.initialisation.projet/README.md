## Initialisation d'un nouveau projet avec DebOps

1. Activer l'environnement virtuel de DebOps :

		source ~/src/debops-venv/bin/activate

1. Créer un nouveau répertoire et y initier un nouveau projet DebOps :

		mkdir ~/src/cargoday.debops
		cd ~/src/cargoday.debops
		debops-init .

1. Ce répertoire dispose maintenant d'une arborescence Ansible de base
similaire à celle présentée dans les [bonnes pratiques][ansible inventaire doc].

		tree
		.
		└── ansible
		    ├── inventory
		    │   ├── group_vars  <= Contiendra les fichiers (ou sous-dossiers) pour
		    │   │   │              définir des variables par groupes
		    │   │   └── all     <= Variables pour tous les hôtes de l'inventaire
		    │   ├── hosts       <= Définition des machines à administrer avec DebOps
		    │   └── host_vars   <= Contiendra les fichiers avec les variables de propres à chaques hôtes
		    ├── playbooks       <= Playbooks supplémentaires
		    └── roles           <= Rôles supplémentaires


<!--Liste des URLs utilisées dans le doc :-->
[ansible inventaire doc]: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#splitting-out-vars
