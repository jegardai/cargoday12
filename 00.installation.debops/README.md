## Installation de DebOps

La [documentation de DebOps indique différentes méthodes d'installation][debops doc installation],
ici je vais l'installer avec un environnement virtuel de Python.

1. Installation des dépendances :

		sudo aptitude install build-essential python3-virtualenv virtualenv python3-dev libffi-dev libssl-dev libsasl2-dev libldap2-dev python3-pip

1. Création d'un répertoire dédié à l'environnement virtuel :

		mkdir -p -- ~/src
		virtualenv ~/src/debops-venv
		cd ~/src/debops-venv
		source bin/activate

1. Mise à jour de quelques paquets Python puis installation de Ansible
et DebOps avec les dernières versions de quelques paquets Python :

		python3 -m pip install --upgrade pip setuptools wheel
		python3 -m pip install ansible debops dnspython future netaddr passlib pyopenssl python-ldap

1. Désactiver ensuite l'environnement virtuel :

		deactivate

* Il y a ensuite plusieurs façons d'utiliser DebOps :
  * En activant l'environnement virtuel Python (qui sera utilisée
    dans la suite de ce tuto) :
    `source ~/src/debops-venv/bin/activate`
  * En plaçant le répertoire **~/src/debops-venv/bin** dans son
    **$PATH** (via ~/.zshrc, ~/.bashrc,…).
  * En créant des liens symboliques depuis un répertoire présent dans le
    $PATH (ex. ~/bin) qui pointent vers les binaires qui nous intéressent
    (debops, ansible, ansible-vault,…).
  * …

<!--Liste des URLs utilisées dans le doc :-->
[debops doc installation]: https://docs.debops.org/en/master/introduction/install.html
