# DebOps pour CargoDay 12

Petite présentation de [DebOps][debops url] pour le [CargoDay12 du 2021/10/21][cargoday12 mathrice url].

## Introduction

DebOps

DebOps est un wrapper pour Ansible, il comporte entre autre

* une bonne collection de rôles Ansible (~180)
* des playbooks pour mettre en place divers services à partir des
  rôles disponibles
* un ensemble de commandes `debops*` calquées sur les commandes
  Ansible pour faciliter l’utilisation des rôles et des playbooks
  proposés
* une documentation bien fournie


## Utilisation à l'IPR

DebOps est notre gestionnaire de configuration pour nos différents
serveurs :

* ~20 conteneurs LXC
	* Serveurs de licence FlexLM
	* Applis web (Apache, Nginx, PHP,…)
	* …
* ~10 VM Qemu
	* Partage NFS
	* Applis web en haute dispo
	* Serveurs de logs
* ~10 serveurs physiques
	* Nœuds Proxmox
		* Il n'y a "que" la mise en cluster qui est manuelle.
	* Nœuds Ceph
* ~60 nœuds de calcul
* quelques postes utilisateurs


## Environnement

* cargoday.ipr : Une machine virtuelle en Debian 11 − Bullseye qui
  me sert à lancer les différentes commandes `debops*`.
* bullseye.ipr : Une machine virtuelle en Debian 11 − Bullseye,
  fraîchement installée via PXE avec [application d'un
  preseed][ipr preseed bullseye ipr].


## Rejouer les sessions

J'ai utilisé l'outil **script** (avec la commande
`script --log-timing script.tm script.log`
) pour enregistrer les sessions shell avec les différentes commandes
et résultats attendus.

* Dans chaque dossier vous pourrez rejouer les sessions avec la commande :

		scriptreplay --log-timing script.tm script.log
* Pour augmenter la vitesse, vous pouvez utiliser l'argument
  **--divisor=2** (2 ou plus) :

		scriptreplay --divisor=2 --log-timing script.tm script.log
* Sous Debian et dérivées, **script** est disponible dans le
  paquet **bsdutils**.
* Si vous n'avez pas l'outil `scriptreplay`, les différents fichiers
  README.md présents dans chaque sous-dossiers reprennent les
  commandes et les principaux résultats attendus.


## Sources

* Le support de présentation est disponible dans le dossier
  [support](support/). Préférer la version **odp** qui contient les
  animations et les logos en entiers…
* Nicolas Quiniou-Briand a publié un article GNU/Linux Magazine
  France en accès libre (n°232 − décembre 2019) :
  [Toute votre infrastructure Debian en YAML avec DebOps][article debops glmf].
	* L'article est un peu ancien mais les bases et l'utilisation sont
	  toujours les mêmes.
* Les sources de DebOps sont disponibles sur [Github][debops github].
* [La documentation DebOps][debops url].


<!--Liste des URLs utilisées dans le doc :-->
[cargoday12 mathrice url]: https://indico.mathrice.fr/event/266/
[debops url]: https://docs.debops.org/en/master/
[debops github]: https://github.com/debops/debops
[ipr preseed bullseye ipr]: https://git.ipr.univ-rennes.fr/cellinfo/tftpboot/src/branch/master/preseed/debian/bullseye/preseed.cfg
[article debops glmf]: https://connect.ed-diamond.com/GNU-Linux-Magazine/glmf-232/toute-votre-infrastructure-debian-en-yaml-avec-debops
