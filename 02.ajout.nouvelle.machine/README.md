## Ajout de la gestion d'une nouvelle machine

1. Activer l'environnement virtuel de DebOps et se placer à la racine
de son projet DebOps :

        source ~/src/debops-venv/bin/activate
        cd ~/src/cargoday.debops/ansible

1. Éditer le fichier **ansible/inventory/hosts** et ajouter une
nouvelle machine dans le groupe **debops\_all\_hosts** :

        [debops_all_hosts]
        bullseye.ipr              ansible_ssh_host=bullseye.ipr.univ-rennes1.fr

1. Créer une clef SSH pour son utilisateur si ce n'est pas déjà
fait :

        ssh-keygen -t ed25519

1. S'assurer que l'utilisateur actuel est bien autorisé à se
connecter en tant que **root** via SSH avec sa clef privée :

        ANS_HOST_FQDN="bullseye.ipr.univ-rennes1.fr
        ANS_HOST="bullseye.ipr
        ssh-copy-id root@"${ANS_HOST_FQDN:=/dev/null}"

        /bin/ssh-copy-id: INFO: Source of key(s) to be installed: "~/.ssh/id_ed25519.pub"
        The authenticity of host 'bullseye.ipr.univ-rennes1.fr (WWW.XXX.YYY.ZZZ)' can't be established.
        ECDSA key fingerprint is SHA256:HxPXY3xoXHzEynZA3lppYueGXds77hLxXRawWmSeVWY.
        Are you sure you want to continue connecting (yes/no/[fingerprint])? yes                     <=========
        /bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
        /bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
        root@bullseye.ipr.univ-rennes1.fr's password:                                                <=========

        Number of key(s) added: 1

        Now try logging into the machine, with:   "ssh 'root@bullseye.ipr.univ-rennes1.fr'"
        and check to make sure that only the key(s) you wanted were added.

1. Créer un premier fichier de variables pour toutes les machines
afin d'autoriser les connexions SSH des utilisateurs avec un mot
de passe.

	Le fichier [ansible/inventory/group_vars/all/sshd.yml][sshd file]
	permettra de surcharger les valeurs par défaut des variables du
	rôle **debops.sshd** :

		---

		# vars file for all hosts to configure sshd role

		# Allow password authentication
		sshd__password_authentication: 'yes'


1. Lors de l'ajout d'une nouvelle machine, la première chose à faire
est d'appliquer l'un des playbooks DebOps dédié au **bootstrap** :

	Ces playbooks permettent entre autre d'installer le minimum
	requis pour qu'Ansible et DebOps s'appliquent correctement sur la
	nouvelle machine.
    * `debops bootstrap -u root -l "${ANS_HOST:=/dev/null}"`

      Ce premier playbook **bootstrap** va automatiquement créer un
      premier utilisateur sur la machine distante avec le même nom
      d'utilisateur que celui utilisé pour lancer la commande
      `debops` et y transférer sa clef SSH.

      Cet utilisateur sera également placé dans le groupe **admins**
      et pourra lancer des commandes via `sudo`.

    * `debops bootstrap-ldap -u root -l "${ANS_HOST:=/dev/null}"`

      Ce second playbook **bootstrap-ldap** va configurer une
      authentification LDAP via le rôle **debops.nslcd** et nécessite
      donc une petite configuration supplémentaire via un fichier
      d'inventaire dédié (ex. ansible/inventory/group\_vars/all/nslcd.conf).
      Voir [la documentation dédiée à debops.nslcd][debops doc nslcd]
      et au minimum les paramètres :

        * nslcd\_\_ldap\_binddn
        * nslcd\_\_ldap\_bindpw (possibilité de chiffrer via ansible-vault)
        * nslcd\_\_ldap\_base\_dn
        * Un exemple de fichier de configuration est également disponible
        sous [cargoday.debops/ansible/inventory/group_vars/all/nslcd.yml][nslcd file]


    * Exemple de résultat après l'application d'un playbook bootstrap :

			PLAY RECAP *****************************************************************************
			bullseye.ipr               : ok=250  changed=124  unreachable=0    failed=0    skipped=103  rescued=0    ignored=0

			Install PKI packages ---------------------------------------------------- 9.25s
			Create base directory hierarchy ----------------------------------------- 8.26s
			Flush handlers for PKI -------------------------------------------------- 7.38s
			Install packages for nslcd support -------------------------------------- 6.32s
			Install required APT packages ------------------------------------------- 4.77s
			Install required packages ----------------------------------------------- 4.20s
			Install atd ------------------------------------------------------------- 4.18s
			Install Libuser requested packages -------------------------------------- 4.02s
			Gather Ansible facts if needed ------------------------------------------ 3.91s
			Ensure that requested UNIX system groups exist -------------------------- 3.83s

1. Il est ensuite normalement possible de se connecter en SSH :

		ssh "${ANS_HOST_FQDN}"
		marvin@bullseye.ipr.univ-rennes1.fr's password:
		Creating directory '/home/marvin'.
		Linux bullseye 5.10.0-9-amd64 #1 SMP Debian 5.10.70-1 (2021-09-30) x86_64
		----------------------------------------------------------------------
		This system is managed by Ansible, manual changes will be lost
		----------------------------------------------------------------------
		$ hostname -f
		bullseye.univ-rennes1.fr


### DH params

L'exécution de la toute première commande `debops` peut être un peu
longue car elle va générer des paramètres Diffie-Hellman qui seront
utilisés par défaut une première fois sur les nouvelles machines.

Un job `atd` est ensuite placé sur les machines pour les régénérer
afin qu'ils soient uniques et propres à chaque hôte.

Pour plus d'informations, voir les documentations relatives à :

  * [la configuration d'une nouvelle machine][debops doc configuration machine]
  * [les informations liées au premier lancement de dhparam][debops doc dhparam]


<!--Liste des URLs utilisées dans le doc :-->
[debops doc bootstrap]: https://docs.debops.org/en/master/introduction/getting-started.html#bootstrap-a-new-host
[debops doc nslcd]: https://docs.debops.org/en/master/ansible/roles/nslcd/defaults/main.html#ldap-environment
[sshd file]: cargoday.debops/ansible/inventory/group_vars/all/sshd.yml
[nslcd file]: cargoday.debops/ansible/inventory/group_vars/all/nslcd.yml
[debops doc configurahttps://docs.debops.org/en/master/introduction/getting-started.html#bootstrap-a-new-hosttion machine]: https://docs.debops.org/en/master/introduction/getting-started.html#configure-the-remote-host
[debops doc dhparam]: https://docs.debops.org/en/master/ansible/roles/dhparam/getting-started.html#initial-configuration
